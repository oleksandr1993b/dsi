$('.header__item-btn').on('click', function () {
    $('.header__item-btn').toggleClass('header__item-btn--active');
    $('.menu__list').toggleClass('menu__list--active')
    // $('.menu__mobile-items').toggleClass('menu__mobile-items--active');
})

$(() => {
    const win = $(window);
    const slider = $(".product__items-blocks");

    win.on("load resize", () => {
        if (win.width() < 450) {
            slider.not(".slick-initialized").slick(
                {
                    autoplay: true,
                    infinite: true,
                    speed: 100,
                    arrows: false,
                    dots: true,
                    cssEase: "linear",
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: false

                }
            );
        } else if (slider.hasClass("slick-initialized")) {
            slider.slick("unslick");
        }
    });
});
