const { src, dest, watch, parallel, series } = require('gulp');

const del = require('del');
const webp = require('gulp-webp')
const scss = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const webpHTML = require('gulp-webp-html');
const imagemin = require('gulp-imagemin');
const css_media = require('gulp-group-css-media-queries');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');



function StylesCss() {
    return src('app/scss/style.scss')
        .pipe(scss({ outputStyle: 'expanded' }))
        .pipe(autoprefixer({
            overrideBrowserslist: [
                'last 10 version'
            ],
            cascade: true,
            grid: true
        }))
        .pipe(css_media())
        .pipe(dest('app/css'))
        .pipe(scss({ outputStyle: 'compressed' }))
        .pipe(concat('style.min.css'))
        .pipe(dest('app/css'))
        .pipe(browserSync.stream())
}

function scripts() {
    return src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/slick-carousel/slick/slick.js',
        'app/js/main.js'
    ])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(dest('app/js'))
        .pipe(browserSync.stream())
}

function imagesCompress() {
    return src([
        'app/images/**/*.webp'
    ], { base: 'app/images' })
        .pipe(
            webp({
                quality: 70
            })
        )
        .pipe(dest('dist/images/**/*.webp'))
        .pipe(src([
            'app/images/**/*.jpg',
            'app/images/**/*.jpeg',
            'app/images/**/*.png',
            'app/images/**/*.svg',
            'app/images/**/*.git',
            'app/images/**/*.ico',
            'app/images/**/*.webp',
        ], { base: 'app/images' }))
        .pipe(imagemin(
            [
                imagemin.gifsicle({ interlaced: true }),
                imagemin.mozjpeg({ quality: 75, progressive: true }),
                imagemin.optipng({ optimizationLevel: 5 }),
                imagemin.svgo({
                    plugins: [
                        { removeViewBox: false },
                        { cleanupIDs: false }
                    ]
                })
            ]
        ))
        .pipe(dest('dist/images'))
        .pipe(browserSync.stream())
    // ,integrationWebp()
}

function integrationWebp() {
    return src('app/index.html')
        .pipe(webpHTML())
        .pipe(dest('dist'))
}


function build() {
    return src([
        'app/*.html',
        'app/css/**/*.min.css',
        'app/js/**/*.min.js',
        'app/fonts/**/*'
    ], { base: 'app' })
        .pipe(dest('dist')),
        imagesCompress()
}

function cleanDist() {
    return del('dist')
}

function Browsersync(params) {
    browserSync.init({
        server: {
            baseDir: 'app/'
        },
        port: 8080,
        notify: false,
    });
};

function watching() {
    watch(['app/scss/**/*.scss'], StylesCss);
    watch(['app/js/**/*.js', '!app/js/**/*.min.js'], scripts);
    watch(['app/*.html']).on('change', browserSync.reload);
}


exports.StylesCss = StylesCss;
exports.scripts = scripts;
exports.imagesCompress = imagesCompress;
exports.integrationWebp = integrationWebp;

exports.Browsersync = Browsersync;

exports.watching = watching;
exports.cleanDist = cleanDist;

exports.build = series(cleanDist, build)
exports.default = parallel(watching, StylesCss, scripts, Browsersync);
